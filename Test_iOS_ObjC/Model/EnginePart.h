//
//  EnginePart.h
//  Test_iOS_ObjC
//
//  Created by Tomasz Iniewicz on 7/29/14.
//  Copyright (c) 2014 MethodCity. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EnginePart <NSObject>

@property (nonatomic, readonly, getter = isEngaged) BOOL engaged;

- (void) engage;

- (void) disengage;

@end
