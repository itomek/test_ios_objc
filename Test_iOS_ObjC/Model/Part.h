//
//  MyModel.h
//  Test_iOS
//
//  Created by Tomasz Iniewicz on 7/29/14.
//  Copyright (c) 2014 MethdoCity Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnginePart.h"

@interface Part : NSObject <EnginePart>

@property (nonatomic, readonly, getter = isEngaged) BOOL engaged;

@property (nonatomic, readonly) NSString *name;

- (instancetype) initWithName: (NSString *)name;

- (void) engage;

- (void) disengage;

@end
