//
//  MyModel.m
//  Test_iOS
//
//  Created by Tomasz Iniewicz on 7/29/14.
//  Copyright (c) 2014 MethdoCity Corporation. All rights reserved.
//

#import "Part.h"

@implementation Part

@synthesize engaged = engaged_;

@synthesize name = name_;

- (instancetype) init
{
    return nil;
}

- (instancetype) initWithName:(NSString *)name
{
    self = [super init];
    
    if (self)
    {
        name_ = name;
    }
    
    return self;
}

- (void) engage
{
    engaged_ = YES;
}

- (void) disengage
{
    engaged_ = NO;
}

@end
