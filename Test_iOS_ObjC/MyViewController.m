//
//  MyViewController.m
//  Test_iOS_ObjC
//
//  Created by Tomasz Iniewicz on 7/29/14.
//  Copyright (c) 2014 MethodCity. All rights reserved.
//

#import "MyViewController.h"
#import "PartViewCell.h"
#import "Part.h"

@interface MyViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic) NSMutableArray *parts;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation MyViewController

- (NSMutableArray *) parts
{
    if (!_parts)
    {
        _parts = [[NSMutableArray alloc] init];
        
        NSMutableArray *subParts = [[NSMutableArray alloc] init];
        
        [subParts addObject:[[Part alloc] initWithName:@"Transmission"]];
        [subParts addObject:[[Part alloc] initWithName:@"Converter"]];
        [subParts addObject:[[Part alloc] initWithName:@"Breaks"]];
        
        [_parts addObject:subParts];
    }
    
    return _parts;
}

#pragma mark - UICollectionView Datasource

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSMutableArray *subParts = [self.parts objectAtIndex:section];
    
    return [subParts count];
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.parts count];
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Something" forIndexPath:indexPath];
    
    PartViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Something" forIndexPath:indexPath];
    
    NSMutableArray *section = [self.parts objectAtIndex:indexPath.section];
    
    Part *part = [section objectAtIndex:indexPath.row];
    
    cell.engaged = part.isEngaged;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void) collectionView: (UICollectionView *) collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: select item
    PartViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Something" forIndexPath:indexPath];
    
    NSMutableArray *section = [self.parts objectAtIndex:indexPath.section];
    
    Part *part = [section objectAtIndex:indexPath.row];
    
    if (part.isEngaged)
    {
        [part disengage];
    }
    else
    {
        [part engage];
    }
    
    cell.engaged = YES;
    
    [collectionView reloadData];
}

- (void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: deselect item
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(64, 64);
}

- (UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(32, 16, 32, 16);
}

#pragma mark - Default


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _parts = [[NSMutableArray alloc] init];
        
        [_parts addObject:[[Part alloc] initWithName:@"Transmission"]];
        [_parts addObject:[[Part alloc] initWithName:@"Converter"]];
        [_parts addObject:[[Part alloc] initWithName:@"Breaks"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
