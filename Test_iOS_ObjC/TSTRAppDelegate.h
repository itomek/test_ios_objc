//
//  TSTRAppDelegate.h
//  Test_iOS_ObjC
//
//  Created by Tomasz Iniewicz on 7/4/14.
//  Copyright (c) 2014 MethodCity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSTRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
