//
//  PartViewCell.m
//  Test_iOS_ObjC
//
//  Created by Tomasz Iniewicz on 7/30/14.
//  Copyright (c) 2014 MethodCity. All rights reserved.
//

#import "PartViewCell.h"
#import <UIKit/UIKit.h>

@interface PartViewCell()

@end

@implementation PartViewCell

@synthesize engaged = engaged_;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (UIImageView *) imageView
{
    if (!_imageView)
    {
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gear_off"]];
    }
    
    return _imageView;
}

- (void) setEngaged:(BOOL)on
{
    if (on)
    {
        self.imageView.image = [UIImage imageNamed:@"gear_on"];
    }
    else
    {
        self.imageView.image = [UIImage imageNamed:@"gear_off"];
    }
    
    engaged_ = engaged_ ? NO : YES;
    
    [self setNeedsDisplay];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
