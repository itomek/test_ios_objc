//
//  main.m
//  Test_iOS_ObjC
//
//  Created by Tomasz Iniewicz on 7/4/14.
//  Copyright (c) 2014 MethodCity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSTRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TSTRAppDelegate class]));
    }
}
