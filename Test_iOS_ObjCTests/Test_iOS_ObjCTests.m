//
//  Test_iOS_ObjCTests.m
//  Test_iOS_ObjCTests
//
//  Created by Tomasz Iniewicz on 7/4/14.
//  Copyright (c) 2014 MethodCity. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Test_iOS_ObjCTests : XCTestCase

@end

@implementation Test_iOS_ObjCTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
